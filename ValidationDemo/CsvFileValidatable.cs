﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidationDemo
{
    public class CsvFileValidatable: CsvFile
    {
        private CsvValidator validator;
        public CsvFileValidatable(CsvValidator validator)
        {
            this.validator = validator;
        }

        public bool IsValid()
        {
            return validator.IsValid(this);
        }

        public IEnumerable<string> ValidationErrors()
        {
            return validator.ValidationErrors(this);
        }
    }
}
