﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ValidationDemo
{
    public class CompanyYValidator : CsvValidator
    {
        public override Func<IEnumerable<string>, string>[] GetRules()
        {
            Func<IEnumerable<string>, string>[] rules = 
            {
                row => row.ElementAt(0).IsNumeric(0),
                row => row.ElementAt(1).IsDate(1),
                row => row.Count() == 4 ? string.Empty : string.Format("4 columns are required, but {0} are provided", row.Count()),
            };

            return rules;
        }
    }
}
