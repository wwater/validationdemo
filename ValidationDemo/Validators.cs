﻿using System;
using System.Collections.Generic;

namespace ValidationDemo
{
    public static class Validators
    {
        public static bool IsNumeric(this string value)
        {
            int result;
            return int.TryParse(value, out result);
        }

        public static string IsNumeric(this string value, int columnNr)
        {
            return value.IsNumeric() ? string.Empty : string.Format("column {0} requires a numeric value", columnNr);
        }

        public static bool IsDate(this string value)
        {
            DateTime result;
            return DateTime.TryParse(value, out result);
        }

        public static string IsDate(this string value, int columnNr)
        {
            return value.IsDate() ? string.Empty : string.Format("column {0} requires a date value", columnNr);
        }

        public static bool IsRequired(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        public static string IsRequired(this string value, int columnNr)
        {
            return value.IsRequired() ? string.Empty : string.Format("column {0} is required", columnNr);
        }
    }
}
