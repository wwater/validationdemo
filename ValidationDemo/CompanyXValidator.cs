﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ValidationDemo
{
    public class CompanyXValidator : CsvValidator
    {
        public override Func<IEnumerable<string>, string>[] GetRules()
        {
            Func<IEnumerable<string>, string>[] rules = 
            {
                row => row.ElementAt(0).IsRequired(0),
            };

            return rules;
        }
    }
}
