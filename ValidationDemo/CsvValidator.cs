﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ValidationDemo
{
    public abstract class CsvValidator
    {
        public IEnumerable<string> ValidationErrors(CsvFile csvFile)
        {
            for (int rowNr = 0; rowNr < csvFile.Rows.Count; rowNr++)
            {
                var row = csvFile.Rows[rowNr];

                foreach (var rule in GetRules())
                {
                    var validationError = rule(row);
                    if (!string.IsNullOrEmpty(validationError))
                    {
                        yield return string.Format("Error on row {0}: {1}", rowNr+1, validationError);
                    }
                }
            }
        }

        public bool IsValid(CsvFile csvFile)
        {
            return !ValidationErrors(csvFile).Any();
        }

        public abstract Func<IEnumerable<string>, string>[] GetRules();
    }
}
