﻿using System.Collections.Generic;

namespace ValidationDemo
{
    public class CsvFile
    {
        public CsvFile()
        {
            this.Rows = new List<List<string>>();
        }

        public List<List<string>> Rows { get; set; }

        public void AddRow(params string[] values)
        {
            var row = new List<string>();
            foreach (var value in values)
            {
                row.Add(value);
            }
            this.Rows.Add(row);
        }
    }
}
