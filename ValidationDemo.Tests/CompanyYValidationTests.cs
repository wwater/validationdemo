﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ValidationDemo.Tests
{
    [TestClass]
    public class CompanyYValidationTests
    {
        private CsvFileValidatable csvFile;

        [TestInitialize]
        public void Init()
        {
            this.csvFile = new CsvFileValidatable(new CompanyYValidator());
        }

        [TestMethod]
        public void First_column_must_be_numeric()
        {
            csvFile.AddRow("123", "2013/01/01", "234", "bertje");
            Assert.IsTrue(csvFile.IsValid());
            csvFile.AddRow("asdfasdf", "2013/01/01", "234", "bertje");

            var validationErrors = csvFile.ValidationErrors().ToList();
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual("Error on row 2: column 0 requires a numeric value", validationErrors[0]);
        }

        [TestMethod]
        public void Second_column_must_be_a_date()
        {
            csvFile.AddRow("123", "2013/01/01", "234", "bertje"); 
            Assert.IsTrue(csvFile.IsValid());
            csvFile.AddRow("123", "asdfasdf", "234", "bertje");

            var validationErrors = csvFile.ValidationErrors().ToList();
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual("Error on row 2: column 1 requires a date value", validationErrors[0]);
        }

        [TestMethod]
        public void Four_columns_are_required()
        {
            csvFile.AddRow("123", "2013/01/01", "234");
            Assert.IsFalse(csvFile.IsValid());

            var validationErrors = csvFile.ValidationErrors().ToList();
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual("Error on row 1: 4 columns are required, but 3 are provided", validationErrors[0]);
        }
    }
}
