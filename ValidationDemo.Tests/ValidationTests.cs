﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ValidationDemo.Tests
{
    [TestClass]
    public class ValidationTests
    {
        private CsvFileValidatable csvFile;

        [TestInitialize]
        public void Init()
        {
            this.csvFile = new CsvFileValidatable(new CompanyYValidator());
        }

        [TestMethod]
        public void Validation_errors_show_correct_line_number()
        {
            csvFile.AddRow("123", "2013/01/01", "234", "bertje");
            csvFile.AddRow("123", "2013/01/01", "234", "bertje");
            csvFile.AddRow("123", "2013/01/01", "234", "bertje"); 
            csvFile.AddRow("asdfasdf", "2013/01/01", "234", "bertje");
            
            Assert.AreEqual(1, csvFile.ValidationErrors().Count());
            var validationError = csvFile.ValidationErrors().First();
            Assert.IsTrue(validationError.Contains("Error on row 4:"));
        }
    }
}
