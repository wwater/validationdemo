﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ValidationDemo.Tests
{
    [TestClass]
    public class CompanyXValidationTests
    {
        private CsvFileValidatable csvFile;

        [TestInitialize]
        public void Init()
        {
            this.csvFile = new CsvFileValidatable(new CompanyXValidator());
        }

        [TestMethod]
        public void First_column_is_required()
        {
            csvFile.AddRow("123");
            Assert.IsTrue(csvFile.IsValid());
            csvFile.AddRow("");

            var validationErrors = csvFile.ValidationErrors().ToList();
            Assert.AreEqual(1, validationErrors.Count);
            Assert.AreEqual("Error on row 2: column 0 is required", validationErrors[0]);
        }
    }
}
